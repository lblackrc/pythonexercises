word = input("Escriba una palabra")
wordInv = word[::-1]    # Invertir una cadena
lista = []
listaInv = []
for letter in word:
    lista.append(letter)

for letter in wordInv:
    listaInv.append(letter)

if lista == listaInv:
    print("Es palindromo")
else:
    print("No es palindromo")