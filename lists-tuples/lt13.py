numbers = input("Escriba numeros separados por comas")
lista = []
number = ""
i = 0
while i<len(numbers):
    while (i<len(numbers) and numbers[i].casefold()!=",".casefold()):
        number+=numbers[i]
        i+=1

    lista.append(int(number))
    number = ""
    i+=1

suma = 0
for i in lista:
    suma+=i
    media = suma / len(lista) 