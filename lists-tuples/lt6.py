cursos = ["Matematicas", "Fisica", "Historia", "Quimica", "Lengua"]
notas = []
eliminados = []

for curso in cursos:
    notas.append((curso,int(input("Ingrese la nota del curso " + curso)))) # creando tuplas en el conjunto de notas

for nota in notas:
    if nota[1]>=10:
        eliminados.append(nota)
# no puedes eliminar un elemento de la lista mientras la estas recorriendo, puede haber errores
        
for eliminado in eliminados:
    notas.remove(eliminado)

for nota in notas:
    print(nota)

